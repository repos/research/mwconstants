import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from mwconstants.links import link_to_namespace
from mwconstants.media import parse_image_options
from mwconstants.namespaces import get_language_list_prefixes, prefixes_to_canonical
