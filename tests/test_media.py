from context import parse_image_options


def test_filename_only():
    lang = 'en'
    link = '[[File:name.jpg]]'
    title = 'File:name.jpg'
    caption = None
    options = []
    assert parse_image_options(link, lang) == (title, caption, options)


def test_filename_without_brackets():
    lang = 'en'
    link = 'File:name.jpg'
    title = 'File:name.jpg'
    caption = None
    options = []
    assert parse_image_options(link, lang) == (title, caption, options)


def test_options():
    lang = 'en'
    link = '[[File:name.jpg|thumb|page=2]]'
    title = 'File:name.jpg'
    caption = None
    options = ['thumb', 'page=2']
    assert parse_image_options(link, lang) == (title, caption, options)


def test_options_nonenglish():
    lang = 'fr'
    link = '[[File:name.jpg|droite|page=2]]'
    title = 'File:name.jpg'
    caption = None
    options = ['droite', 'page=2']
    assert parse_image_options(link, lang) == (title, caption, options)

def test_wrong_options():
    lang = 'en'
    link = '[[File:name.jpg|THUMB|page=2]]'
    title = 'File:name.jpg'
    caption = 'THUMB'
    options = ['page=2']
    assert parse_image_options(link, lang) == (title, caption, options)

def test_multiple_captions():
    lang = 'en'
    link = '[[File:name.jpg|THUMB|PAGE=2]]'
    title = 'File:name.jpg'
    caption = 'PAGE=2'
    options = []
    assert parse_image_options(link, lang) == (title, caption, options)
