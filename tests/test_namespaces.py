from context import get_language_list_prefixes


def test_namespace_prefixes():
    lang = 'fr'
    namespace = 'Project'
    assert sorted(get_language_list_prefixes(namespace, lang=lang)) == ['Project', 'WP', 'Wikipedia', 'Wikipédia']
